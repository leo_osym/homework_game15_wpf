﻿using System;
using System.Data;
using System.Data.SQLite;
using System.Data.Sql;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Data.SQLite.Generic;

namespace GameLogic
{
    public class ConnectToDb
    {
        public string DbError { get; set; }
        public void CreateDatabase()
        {
            if (!File.Exists("results.sqlite"))
            {
                try
                {
                    SQLiteConnection.CreateFile("results.sqlite");
                    using (SQLiteConnection m_dbConnection = new SQLiteConnection("Data Source=results.sqlite;Version=3;"))
                    {
                        m_dbConnection.Open();
                        string sql = "CREATE TABLE records (name VARCHAR(64), scored VARCHAR(32), date VARCHAR(32))";
                        SQLiteCommand command = new SQLiteCommand(sql, m_dbConnection);
                        command.ExecuteNonQuery();

                        m_dbConnection.Close();
                    }
                }
                catch (Exception ex)
                {
                    DbError = ex.Message;
                }
            }
        }
        public void LoadToDb(PlayerContainer player)
        {
            try
            {
                using (SQLiteConnection m_dbConnection = new SQLiteConnection("Data Source=results.sqlite;Version=3;"))
                {
                    m_dbConnection.Open();
                    string sql = $"INSERT INTO records (name, scored, date) VALUES ('{player.Name}','{player.Time.ToString()}','{player.Date.ToString()}')";
                    SQLiteCommand command = new SQLiteCommand(sql, m_dbConnection);
                    command.ExecuteNonQuery();

                    m_dbConnection.Close();
                }
            }
            catch (Exception ex)
            {
                DbError = ex.Message;
            }
        }
        public List<PlayerContainer> LoadFromDb()
        {
            List<PlayerContainer> playersList = new List<PlayerContainer>();
            try
            {
                using (SQLiteConnection m_dbConnection = new SQLiteConnection("Data Source=results.sqlite;Version=3;"))
                {
                    m_dbConnection.Open();
                    string sql = "SELECT * FROM records";
                    SQLiteCommand command = new SQLiteCommand(sql, m_dbConnection);
                    SQLiteDataReader reader = command.ExecuteReader();
                    while (reader.Read())
                    {
                        playersList.Add(new PlayerContainer
                        {
                            Name = reader["name"].ToString(),
                            Date = DateTime.Parse(reader["date"].ToString()),
                            Time = TimeSpan.Parse(reader["scored"].ToString())
                        });
                    }
                    m_dbConnection.Close();
                }
            }
            catch (Exception ex)
            {
                DbError = ex.Message;
            }
            return playersList;
        }
    }
}
