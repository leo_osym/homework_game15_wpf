﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameLogic
{
    public class PlayerContainer : IDataErrorInfo, IComparable<PlayerContainer>
    {
        public bool isPlaying { get; set; }
        public string Name { get; set; }
        public TimeSpan Time { get; set; }
        public DateTime Date { get; set; }
        public string this[string columnName]
        {
            get
            {
                string error = String.Empty;
                if (columnName == "Name")
                    if (String.IsNullOrWhiteSpace(Name))
                    {
                        error = "Name should not be empty!";
                        Error = error;
                    }
                    else Error = String.Empty;
                return error;
            }
        }

        public string Error { get; set; } = string.Empty;

        public int CompareTo(PlayerContainer other)
        {
            return TimeSpan.Compare(this.Time, other.Time);
        }
    }
}
