﻿using GameLogic;
using Lesson21_Game15WPF;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Game15_wpf
{
    /// <summary>
    /// Interaction logic for ResultsWindow.xaml
    /// </summary>
    public partial class ResultsWindow : Window
    {
        List<PlayerContainer> players = new List<PlayerContainer>();
        ConnectToDb connect = new ConnectToDb();
        public ResultsWindow()
        {
            InitializeComponent();
            players = connect.LoadFromDb();
            // Add columns
            var gridView = new GridView();
            this.listView.View = gridView;
            gridView.Columns.Add(new GridViewColumn
            {
                Header = "Name",
                DisplayMemberBinding = new Binding("Name")
            });
            gridView.Columns.Add(new GridViewColumn
            {
                Header = "Score",
                DisplayMemberBinding = new Binding("Score")
            });
            gridView.Columns.Add(new GridViewColumn
            {
                Header = "Date",
                DisplayMemberBinding = new Binding("Date")
            });
            // Populate list
            //ListView view = new ListView();
            players.Sort();
            foreach (var player in players)
            {
                var item = new { Name = player.Name, Score = player.Time.ToString(), Date = player.Date.ToString() };
                //view.Items.Add(item);
                this.listView.Items.Add(item);
            }

        }

        private void butStart_Click(object sender, RoutedEventArgs e)
        {
            StartWindow sw = new StartWindow();
            sw.Activate();
            sw.Show();
            Close();
        }

        private void butExit_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }
    }
}
