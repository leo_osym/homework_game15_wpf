﻿using GameLogic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Lesson21_Game15WPF
{
    /// <summary>
    /// Interaction logic for StartWindow.xaml
    /// </summary>
    public partial class StartWindow : Window
    {
        public PlayerContainer player = new PlayerContainer();
        public StartWindow()
        {
            InitializeComponent();
        }

        private void cancelButton_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void startButton_Click(object sender, RoutedEventArgs e)
        {
            player.Name = tbox.Text;
            this.DataContext = player;
            lbelError.Content = player.Error;
            lbelError.Foreground = Brushes.Red;
            if (player.Error.Equals(string.Empty))
            {
                MainWindow mw = new MainWindow();
                mw.player = player;
                mw.Activate();
                mw.Show();
                this.Close();
            }
        }
    }
}
